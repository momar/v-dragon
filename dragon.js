function Dragon(el, payload) {
  this.el = el;
  this.payload = payload;
  el.setAttribute("draggable", "true");
  el.addEventListener("dragstart", this.dragStart = this.dragStart.bind(this), false);
  el.addEventListener("dragend", this.dragEnd = this.dragEnd.bind(this), false);
  el.addEventListener("drag", this.drag = this.drag.bind(this), false);
  el.addEventListener("drop", this.drop = this.drop.bind(this), false);
  el.addEventListener("dragenter", this.dragEnter = this.dragEnter.bind(this), false);
  el.addEventListener("dragover", this.dragOver = this.dragOver.bind(this), false);
  el.addEventListener("dragleave", this.dragLeave = this.dragLeave.bind(this), false);
  el.addEventListener("mouseover", this.dragEnable = this.dragEnable.bind(this), false);
  el.addEventListener("mouseleave", this.dragDisable = this.dragDisable.bind(this), false);
};

Dragon.state = {};

Dragon.directive = function() {
  const dir = {};
  dir.bind = function(el, binding, vnode) {
      vnode.dragon = new Dragon(el, function() {
          return this.data.attrs["dragon-payload"];
      }.bind(vnode));
  }
  dir.update = function(el, binding, vnode, oldVnode) {
      vnode.dragon = oldVnode.dragon;
      vnode.dragon.payload = function() {
          return this.data.attrs["dragon-payload"];
      }.bind(vnode);
  }
  dir.unbind = function(el, binding, vnode) {
      vnode.dragon.unbind();
  }
  return dir;
}

Dragon.install = function(Vue) {
  Vue.directive("dragon", Dragon.directive);
}

Dragon.prototype.unbind = function() {
  this.el.classList.remove("dragon-use", "dragon-aim");
  this.el.removeAttribute("draggable");
  this.el.removeEventListener("dragstart", this.dragStart);
  this.el.removeEventListener("dragend", this.dragEnd);
  this.el.removeEventListener("drag", this.drag);
  this.el.removeEventListener("drop", this.drop);
  this.el.removeEventListener("dragenter", this.dragEnter);
  this.el.removeEventListener("dragover", this.dragOver);
  this.el.removeEventListener("dragleave", this.dragLeave);
  this.el.removeEventListener("mouseover", this.mouseOver);
  this.el.removeEventListener("mouseleave", this.mouseLeave);
}

Dragon.prototype.emit = function(from, to, keys, clientX, clientY) {
  var eventObject = {
          from: from,
          to: to,
          keyCtrl: !!keys.ctrl,
          keyAlt: !!keys.alt,
          keyShift: !!keys.shift,
          mouseLeft: !!keys.left,
          mouseMiddle: !!keys.middle,
          mouseRight: !!keys.right,
          clientX: clientX,
          clientY: clientY,
      }
      //vnode.context.$emit("dragon-drop", eventObject);
  var ev = new Event("dragon-drag");
  ev.data = eventObject;
  Dragon.state.startElement.dispatchEvent(ev);
  ev = new Event("dragon-drop");
  ev.data = eventObject;
  this.el.dispatchEvent(ev);
}
Dragon.prototype.dragStart = function() {
  Dragon.state.startPayload = this.payload();
  Dragon.state.startElement = this.el;
  window.event.cancelBubble = true;
  window.event.dataTransfer.setData("text", JSON.stringify(this.payload()));
}
Dragon.prototype.dragEnd = function() {

}
Dragon.prototype.drag = function() {

}
Dragon.prototype.drop = function() {
  window.event.cancelBubble = true;
  window.event.preventDefault();
  this.emit(Dragon.state.startPayload, this.payload(), {}, window.event.clientX, window.event.clientY);

  var aims = document.getElementsByClassName("dragon-aim");
  for (var i = 0; i < aims.length; i++) aims[i].classList.remove("dragon-aim");
}
Dragon.prototype.dragEnter = function() {

}
Dragon.prototype.dragOver = function() {
  window.event.cancelBubble = true;
  window.event.preventDefault();
  var aims = document.getElementsByClassName("dragon-aim");
  for (var i = 0; i < aims.length; i++) aims[i].classList.remove("dragon-aim");
  this.el.classList.add("dragon-aim");
}
Dragon.prototype.dragLeave = function() {
  this.el.classList.remove("dragon-aim");
}

Dragon.prototype.dragEnable = function() {
  console.log(window.event);
  var el = window.event.target;
  while (el && el != window && el != this.el && !el.hasAttribute("dragon-handle")) {
      el = el.parentElement;
  }
  if (!el || el == window || (el == this.el && !el.hasAttribute("dragon-handle") && el.querySelector("[dragon-handle]") != null)) {
    this.el.removeAttribute("draggable");
    return;
  }
  this.el.setAttribute("draggable", "true");
}
Dragon.prototype.dragDisable = function() {
  this.el.removeAttribute("draggable");
}

if (typeof module == "object") module.exports = Dragon;
else if (typeof define == "function" && define.amd) define([], function() { return DragAndDrop });
else if (window.Vue) { window.Dragon = Dragon; Vue.use(Dragon); }
